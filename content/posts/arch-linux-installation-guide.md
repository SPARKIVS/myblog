---
title: "Arch Linux Installation Manual"
date: "2019-06-24"
---

### 1. 基本系统

#### 验证启动模式

如果以在 [UEFI](https://wiki.archlinux.org/index.php/UEFI) 主板上启用 [UEFI](https://wiki.archlinux.org/index.php/UEFI) 模式，[Archiso](https://wiki.archlinux.org/index.php/Archiso) 将会使用 [systemd-boot](<https://wiki.archlinux.org/index.php/Systemd-boot_(简体中文)>) 来 [启动](https://wiki.archlinux.org/index.php/Boot) Arch Linux。可以列出 [efivars](https://wiki.archlinux.org/index.php/UEFI#UEFI_variables) 目录以验证启动模式：

```bash
ls /sys/firmware/efi/efivars
```

#### 更新系统时间

使用 [timedatectl(1)](https://jlk.fjfi.cvut.cz/arch/manpages/man/timedatectl.1) 确保系统时间是准确的：

```bash
timedatectl set-ntp true
```

#### 连接到因特网

通过 `ip link` 查看无线设备名，通常是类似 `wlp2s1`的设备。

1. 启用设备：

```bash
ip link set wlp56s0f3u4 up
```

2. 连接无线网络：

```bash
wifi-menu -o
```

3. 用 ping 检查网络连接：

```bash
ping archlinux.org
```

#### 建立硬盘分区

磁盘若被系统识别到，就会被分配为一个 [块设备](https://en.wikipedia.org/wiki/zh:设备文件系统#.E5.91.BD.E5.90.8D.E7.BA.A6.E5.AE.9A)，如 `/dev/sda` 或者 `/dev/nvme0n1`。可以使用 `lsblk -l`或者 `fdisk -l`查看：

```bash
lsblk -l
```

分区示例：

```bash
NAME MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0  7:0    0 500.8G  1 loop /run/archiso/sfs/airootfs
sda    8:0    0 223.6G  0 disk
sda1   8:1    0   512M  0 part /mnt/boot/efi
sda2   8:2    0     4G  0 part [SWAP]
sda3   8:3    0    64G  0 part /mnt
sda4   8:4    0    64G  0 part /mnt/opt
sdb5   8:5    0  91.9G  0 part /mnt/home
sdb    8:16   1  14.7G  0 disk
sdc1   8:17   1 119.2G  0 part /run/archiso/bootmnt
```

开始分区（GPT 硬盘使用 gdisk 分区）：

```bash
gdisk /dev/sda
```

第一个分区`+512M`，第二个分区`+4G`，第三个分区`+64G`，第四个分区`+64G`，第五个分区默认剩下的空间。

#### 格式化分区

格式化第一块分区（挂载点`/mnt/boot/EFI`）：

```bash
mkfs.fat -F32 /dev/sda1
```

初始化交换分区：

```bash
mkswap /dev/sda2
swapon /dev/sda2
```

格式化第三块分区（挂载点`/mnt`）：

```bash
mkfs.ext4 /dev/sda3
```

格式化第四块分区（挂载点`/mnt/opt`）：

```bash
mkfs.ext4 /dev/sda4
```

格式化第五块分区（挂载点`/mnt/home`）：

```bash
mkfs.ext4 /dev/sda5
```

#### 挂载分区

先将根分区挂载到`/mnt`：

```bash
mount /dev/sda3 /mnt
```

创建其他剩余的挂载点：

```bash
mkdir -p /mnt/boot/EFI
mkdir /mnt/opt
mkdir /mnt/home
```

再将 boot 分区挂载到`/mnt/boot/EFI`：

```bash
mount /dev/sda1 /mnt/boot/EFI
```

再将 opt 分区挂载到`/mnt/opt`：

```bash
mount /dev/sda4 /mnt/opt
```

最后将 home 分区挂载到`/mnt/home`：

```bash
mount /dev/sda5 /mnt/home
```

#### 选择镜像：

文件 `/etc/pacman.d/mirrorlist` 定义了软件包会从哪个 [镜像源](https://wiki.archlinux.org/index.php/Mirrors) 下载。在 LiveCD 启动的系统上，所有的镜像都被启用，并且通过他们的同步情况和速度进行了排序。在列表中越前的镜像在下载软件包时有越高的优先权。相应的修改文件 `/etc/pacman.d/mirrorlist`，并将地理位置最近的镜像源挪到文件的头部，这个文件接下来还会被 _pacstrap_ 拷贝到新系统里，所以请确保设置正确。

```bash
vim /etc/pacman.d/mirrorlist
```

按<kbd>i</kbd>进入编辑模式，添加：

```
Server = https://mirrors.tuna.tsinghua.edu.cn/archlinux/$repo/os/$arch
Server = https://mirrors.ustc.edu.cn/archlinux/$repo/os/$arch
```

按 <kbd>ESC</kbd> 退出编辑模式，按<kbd>:x</kbd>保存并退出。

#### 安装基本系统

```bash
pacstrap /mnt base base-devel vim git neofetch dialog wpa-supplicant
```

#### Fstab

用以下命令生成 [fstab](https://wiki.archlinux.org/index.php/Fstab) 文件 (用 `-U` 或 `-L` 选项设置 UUID 或卷标)：

```bash
genfstab -U /mnt >> /mnt/etc/fstab
```

```bash
vim /mnt/etc/fstab
```

按ｉ进入编辑模式，把 EFI 分区移动到第一位，SSD 添加参数`noatime,discard`：

修改前：

```bash
# Static information about the filesystems.
# See fstab(5) for details.

# <file system>             <mount point>  <type>  <options>  <dump>  <pass>
# /dev/sda3
UUID=81cf250c               /              ext4    rw,relatime 0 1
# /dev/sda1
UUID=52C6-2A8               /boot/EFI      vfat    rw,relatime 0 2
# /dev/sda4
UUID=0222c134               /opt           ext4    rw,relatime 0 2
# /dev/sda5
UUID=8b57951e               /home          ext4    rw,relatime 0 2
# /dev/sda2
UUID=06f8ec54               none           swap    defaults    0 0
```

修改后：

```bash
# Static information about the filesystems.
# See fstab(5) for details.

# <file system>             <mount point>  <type>  <options>  <dump>  <pass>
# /dev/sda1
UUID=52C6-2A8               /boot/EFI      vfat    rw,relatime,noatime,discard 0 2
# /dev/sda3
UUID=81cf250c               /              ext4    rw,relatime,noatime,discard 0 1
# /dev/sda4
UUID=0222c134               /opt           ext4    rw,relatime,noatime,discard 0 2
# /dev/sda5
UUID=8b57951e               /home          ext4    rw,relatime,noatime,discard 0 2
# /dev/sda2
UUID=06f8ec54               none           swap    defaults,noatime,discard    0 0
```

按 <kbd>ESC</kbd>退出编辑模式，按<kbd>:x</kbd>保存并退出。

#### Chroot

[Change root](https://wiki.archlinux.org/index.php/Change_root) 到新安装的系统：

```bash
arch-chroot /mnt
```

#### 时区

设置 [时区](https://wiki.archlinux.org/index.php/Time_zone)：

```bash
ln -sf /usr/share/zoneinfo/Region/City /etc/localtime
```

例如：

```bash
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
```

运行 [hwclock(8)](https://jlk.fjfi.cvut.cz/arch/manpages/man/hwclock.8) 以生成 `/etc/adjtime`：

```bash
hwclock --systohc
```

这个命令假定硬件时间已经被设置为 [UTC 时间](https://en.wikipedia.org/wiki/UTC)。详细信息请查看 [System time#Time standard](https://wiki.archlinux.org/index.php/System_time#Time_standard)。

#### 本地化

本地化的程序与库若要本地化文本，都依赖 [Locale](https://wiki.archlinux.org/index.php/Locale)，后者明确规定地域、货币、时区日期的格式、字符排列方式和其他本地化标准等等。在下面两个文件设置：`locale.gen` 与 `locale.conf`。

`/etc/locale.gen` 是一个仅包含注释文档的文本文件。指定您需要的本地化类型，只需移除对应行前面的注释符号（`＃`）即可，建议选择带 `UTF-8` 的项：

```bash
vim /etc/locale.gen
en_US.UTF-8 UTF-8
zh_CN.UTF-8 UTF-8
zh_TW.UTF-8 UTF-8
```

按<kbd>/</kbd>搜索`en_US.UTF-8 UTF-8`，按<kbd>i</kbd>编辑，删除前面的注释符号＃

按<kbd>/</kbd>搜索`en_CN.UTF-8 UTF-8`，按<kbd>i</kbd>编辑，删除前面的注释符号＃

按<kbd>/</kbd>搜索`en_TW.UTF-8 UTF-8`，按<kbd>i</kbd>编辑，删除前面的注释符号＃

按<kbd>ESC</kbd>退出编辑模式，按<kbd>:x</kbd>保存并退出。

接着执行：

```bash
locale-gen
```

```bash
vim /etc/locale.conf
```

按<kbd>i</kbd>编辑，写入`LANG=en_US.UTF-8`，按<kbd>ESC</kbd>退出编辑模式，按<kbd>:x</kbd>保存并退出。

#### 网络

创建 [hostname](https://wiki.archlinux.org/index.php/Hostname) 文件：

```bash
vim /etc/hostname
```

按<kbd>i</kbd>编辑，写入主机名`c3po`，按<kbd>ESC</kbd>退出编辑模式，按<kbd>:x</kbd>保存并退出。

添加对应的信息到 [hosts(5)](https://jlk.fjfi.cvut.cz/arch/manpages/man/hosts.5):

```
/etc/hosts
127.0.0.1	localhost
::1			localhost
127.0.1.1	c3po.localdomain	c3po
```

#### Root 密码

设置 Root [密码](https://wiki.archlinux.org/index.php/Password)：

```bash
passwd
```

#### 安装引导程序

```bash
pacman -S dosfstools grub efibootmgr amd-ucode
```

```bash
grub-install --target=x86_64-efi --efi-directory=/boot/EFI --bootloader-id=grub
```

```bash
grub-mkconfig -o /boot/grub/grub.cfg
```

#### 重启系统

输入 `exit` 或按 <kbd>Ctrl</kbd> + <kbd>D</kbd> 退出 chroot 环境。

用 `umount -R /mnt` 手动卸载被挂载的分区，有助于发现可能任何「繁忙」的分区。

最后，通过执行 `reboot` 重启系统，_systemd_ 将自动卸载仍然挂载的任何分区。不要忘记移除安装介质，然后使用 root 帐户登录到新系统。

### 2. 桌面环境（以 Deepin Desktop Environment 为例）

#### 连接到因特网

通过 `ip link` 查看无线设备名，通常是类似 `wlp2s1`的设备。

1. 启用设备：

```bash
ip link set wlp56s0f3u4 up
```

2. 连接无线网络：

```bash
wifi-menu -o
```

3. 用 ping 检查网络连接：

```bash
ping archlinux.org
```

#### 添加登录用户

以典型的桌面系统为例，要添加一个名为**_[用户名]_**的用户，并使用[bash](<https://wiki.archlinux.org/index.php/Bash_(简体中文)>)作为登录 shell：

```bash
useradd -m -G wheel -s /bin/bash [用户名]
```

设置用户密码：

```bash
passwd [用户名]
```

```bash
visudo
```

往下翻，找到这一行：

```bash
## Uncomment to allow members of group wheel to execute any command
# %wheel ALL=(ALL) ALL
```

把`# %wheel ALL=(ALL) ALL`前的注释符号＃删掉：

```bash
## Uncomment to allow members of group wheel to execute any command
%wheel ALL=(ALL) ALL
```

<kbd>Ctrl</kbd>＋<kbd>S</kbd> 保存修改，<kbd>Ctrl</kbd>＋<kbd>X</kbd> 退出。

#### 安装显示服务

```bash
pacman -S xorg xorg-server networkmanager
```

#### 安装 Deepin 桌面环境

```bash
pacman -S deepin deepin-extra
```

#### 配置 lightdm 登录管理器

```bash
vim /etc/lightdm/lightdm.conf
```

找到这一行，按**ｉ**编辑：

```bash
#greeter-session=example-gtk-gnome
```

删掉注释符号＃，并改为：

```bash
greeter-session=lightdm-deepin-greeter
```

按 **ESC** 退出编辑模式，按：ｘ保存并退出。

启用登录管理器：

```bash
systemctl enable lightdm.service
```

启用网络管理器，重启系统：

```bash
systemctl enable NetworkManager.service
reboot
```

### 3. Test

#### Latex:

$$
\mathbf{V}_1 \times \mathbf{V}_2 =  \begin{vmatrix}
\mathbf{i} & \mathbf{j} & \mathbf{k} \\
\frac{\partial X}{\partial u} &  \frac{\partial Y}{\partial u} & 0 \\
\frac{\partial X}{\partial v} &  \frac{\partial Y}{\partial v} & 0 \\
\end{vmatrix}
$$

#### Plots:

1. 横向流程图源码格式：

```mermaid
graph LR
A[方形] -->B(圆角)
    B --> C{条件a}
    C -->|a=1| D[结果1]
    C -->|a=2| E[结果2]
    F[横向流程图]
```

2. 竖向流程图源码格式：

```mermaid
graph TD
A[方形] --> B(圆角)
    B --> C{条件a}
    C --> |a=1| D[结果1]
    C --> |a=2| E[结果2]
    F[竖向流程图]
```

3. 标准流程图源码格式：

```flow
st=>start: 开始框
op=>operation: 处理框
cond=>condition: 判断框(是或否?)
sub1=>subroutine: 子流程
io=>inputoutput: 输入输出框
e=>end: 结束框
st->op->cond
cond(yes)->io->e
cond(no)->sub1(right)->op
```

4. 标准流程图源码格式（横向）：

```flow
st=>start: 开始框
op=>operation: 处理框
cond=>condition: 判断框(是或否?)
sub1=>subroutine: 子流程
io=>inputoutput: 输入输出框
e=>end: 结束框
st(right)->op(right)->cond
cond(yes)->io(bottom)->e
cond(no)->sub1(right)->op
```

5. UML 时序图源码样例：

```sequence
对象A->对象B: 对象B你好吗?（请求）
Note right of 对象B: 对象B的描述
Note left of 对象A: 对象A的描述(提示)
对象B-->对象A: 我很好(响应)
对象A->对象B: 你真的好吗？
```

6. UML 时序图源码复杂样例：

```sequence
Title: 标题：复杂使用
对象A->对象B: 对象B你好吗?（请求）
Note right of 对象B: 对象B的描述
Note left of 对象A: 对象A的描述(提示)
对象B-->对象A: 我很好(响应)
对象B->小三: 你好吗
小三-->>对象A: 对象B找我了
对象A->对象B: 你真的好吗？
Note over 小三,对象B: 我们是朋友
participant C
Note right of C: 没人陪我玩
```

7. UML 标准时序图样例：

```mermaid
%% 时序图例子,-> 直线，-->虚线，->>实线箭头
  sequenceDiagram
    participant 张三
    participant 李四
    张三->王五: 王五你好吗？
    loop 健康检查
        王五->王五: 与疾病战斗
    end
    Note right of 王五: 合理 食物 <br/>看医生...
    李四-->>张三: 很好!
    王五->李四: 你怎么样?
    李四-->王五: 很好!
```

8. 甘特图样例：

```mermaid
%% 语法示例
        gantt
        dateFormat  YYYY-MM-DD
        title 软件开发甘特图
        section 设计
        需求                      :done,    des1, 2014-01-06,2014-01-08
        原型                      :active,  des2, 2014-01-09, 3d
        UI设计                     :         des3, after des2, 5d
    未来任务                     :         des4, after des3, 5d
        section 开发
        学习准备理解需求                      :crit, done, 2014-01-06,24h
        设计框架                             :crit, done, after des2, 2d
        开发                                 :crit, active, 3d
        未来任务                              :crit, 5d
        耍                                   :2d
        section 测试
        功能测试                              :active, a1, after des3, 3d
        压力测试                               :after a1  , 20h
        测试报告                               : 48h
```
