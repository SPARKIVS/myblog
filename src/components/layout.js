import React from "react"
// import { css } from "@emotion/core"
import { useStaticQuery, Link, graphql } from "gatsby"
import styled, { css, keyframes } from "styled-components"
import { rhythm } from "../utils/typography"
import "../fonts/fonts.css"

const bounce = keyframes`
  from, 20%, 53%, 80%, to {
    transform: translate3d(0,0,0);
  }

  40%, 43% {
    transform: translate3d(0, -30px, 0);
  }

  70% {
    transform: translate3d(0, -15px, 0);
  }

  90% {
    transform: translate3d(0,-4px,0);
  }
`

const animation = css`
  ${bounce} 1s ease infinite;
`

const Title = styled.h2`
  margin-bottem: ${rhythm(2)};
  display: inline-block;
  font-style: normal;
  font-family: Happy;
  &:hover {
    animation: ${animation};
  }
`

const Wrapper = styled.div`
  min-height: 100vh;
`

const Footer = styled.footer`
  text-align: center;
  margin: 24px;
`

const ListLink = props => (
  <li
    css={css`
      display: inline-block;
      margin-right: 1rem;
      font-family: Happy;
      font-size: 0.9rem;
    `}
  >
    <Link to={props.to}>{props.children}</Link>
  </li>
)

export default function Layout({ children }) {
  const data = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
          }
        }
      }
    `
  )
  return (
    <Wrapper>
      <div
        css={css`
          margin: 0 auto;
          max-width: 800px;
          padding: ${rhythm(2)};
          padding-top: ${rhythm(2)};
        `}
      >
        <header>
          <Link to={`/`}>
            <Title>{data.site.siteMetadata.title}</Title>
          </Link>
          <ul
            css={css`
              list-style: none;
              float: right;
              margin-top: 0.3rem;
              display: flex;
            `}
          >
            <ListLink to="/">Home</ListLink>
            <ListLink to="/archives/">Archives</ListLink>
            <ListLink to="/about/">About</ListLink>
          </ul>
        </header>
        <main>{children}</main>
      </div>
      <Footer>
        © {new Date().getFullYear()}, Build with{" "}
        <div
          css={css`
            display: inline;
            vertical-align: text-top;
            postion: relative;
            align-items: stretch;
            cursor: pointer;
          `}
        >
          <svg width="16" height="16" viewBox="0 0 24 24">
            <g>
              <path d="M12 21.638h-.014C9.403 21.59 1.95 14.856 1.95 8.478c0-3.064 2.525-5.754 5.403-5.754 2.29 0 3.83 1.58 4.646 2.73.814-1.148 2.354-2.73 4.645-2.73 2.88 0 5.404 2.69 5.404 5.755 0 6.376-7.454 13.11-10.037 13.157H12zM7.354 4.225c-2.08 0-3.903 1.988-3.903 4.255 0 5.74 7.034 11.596 8.55 11.658 1.518-.062 8.55-5.917 8.55-11.658 0-2.267-1.823-4.255-3.903-4.255-2.528 0-3.94 2.936-3.952 2.965-.23.562-1.156.562-1.387 0-.014-.03-1.425-2.965-3.954-2.965z"></path>
            </g>
          </svg>
        </div>
        , powered by{" "}
        <a href="https://www.gatsbyjs.org">
          <span
            css={css`
              margin-right: 0.2rem;
              display: inline-block;
              vertical-align: text-top;
            `}
          >
            <svg width="16" height="16" viewBox="0 0 512.000000 512.000000">
              <g
                transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)"
                fill="#000000"
                stroke="none"
              >
                <path
                  d="M2225 5109 c-980 -126 -1810 -821 -2113 -1771 -85 -265 -112 -453
-112 -778 0 -325 27 -513 112 -778 305 -956 1138 -1650 2128 -1772 109 -13
531 -13 640 0 990 122 1823 816 2128 1772 85 265 112 453 112 778 0 325 -27
513 -112 778 -305 956 -1138 1650 -2128 1772 -100 12 -555 11 -655 -1z m597
-554 c398 -53 776 -227 1077 -496 102 -92 281 -292 281 -315 0 -12 -258 -234
-271 -234 -4 0 -22 19 -40 43 -249 320 -586 533 -983 619 -87 19 -131 22 -326
22 -195 0 -239 -3 -326 -22 -549 -119 -989 -488 -1198 -1002 l-26 -65 1048
-1047 1047 -1048 65 26 c376 152 680 434 864 803 42 83 103 247 120 324 l6 27
-435 0 -435 0 0 185 0 185 640 0 640 0 0 -74 c0 -174 -52 -435 -125 -631 -219
-588 -721 -1052 -1324 -1226 l-113 -32 -1205 1206 -1206 1205 32 113 c218 756
878 1327 1656 1433 137 19 399 19 537 1z m-302 -4003 c-6 -2 -65 1 -133 7
-979 90 -1728 831 -1827 1810 -5 57 -10 117 -10 135 0 28 83 -52 990 -959 545
-545 985 -992 980 -993z"
                />
              </g>
            </svg>
          </span>
          Gatsby
        </a>
      </Footer>
    </Wrapper>
  )
}
