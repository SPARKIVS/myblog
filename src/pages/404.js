import React from "react"
import { css } from "@emotion/core"
import Layout from "../components/layout"

export default function Notfind() {
  return (
    <Layout>
      <div>
        <img src="/404.svg" alt="404"></img>
      </div>
      <h1
        css={css`
          display: inline-block;
          font-family: Happy;
          font-size: 2rem;
        `}
      >
        >_ Loading ...
      </h1>
    </Layout>
  )
}
