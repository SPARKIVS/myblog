import React from "react"
import { graphql } from "gatsby"
import { css } from "@emotion/core"
import Layout from "../components/layout"
import Bio from "../components/bio"
import SEO from "../components/seo"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
  faGitlab,
  faGithub,
  faTwitter,
  faKeybase,
  faTelegram,
} from "@fortawesome/free-brands-svg-icons"

const LiLink = props => (
  <li>
    <a href={props.to}>
      <FontAwesomeIcon
        css={css`
          margin-right: 0.5rem;
        `}
        icon={props.icon}
      />
      {props.children}
    </a>
  </li>
)

export default function About({ data }) {
  return (
    <Layout>
      <SEO title="About" />
      <h1
        css={css`
          display: inline-block;
          font-family: Merriweather, Georgia, serif;
        `}
      >
        About
      </h1>
      <Bio />
      <p>
        We're the only site running on your computer dedicated to showing the
        best photos and videos of pandas eating lots of food. N0rmie
      </p>
      <ul>
        <LiLink icon={faGitlab} to={"https://gitlab.com"}>
          Gitlab
        </LiLink>
        <LiLink icon={faGithub} to={"https://github.com"}>
          Github
        </LiLink>
        <LiLink icon={faTwitter} to={"https://twitter.com"}>
          Twitter
        </LiLink>
        <LiLink icon={faKeybase} to={"https://keybase.io"}>
          Keybase
        </LiLink>
        <LiLink icon={faTelegram} to={"https://telegram.pl"}>
          Telegram
        </LiLink>
      </ul>
    </Layout>
  )
}

export const query = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`
