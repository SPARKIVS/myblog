import React from "react"
import { css } from "@emotion/core"
import Layout from "../components/layout"
import SEO from "../components/seo"

export default function Archives() {
  return (
    <Layout>
      <SEO title="Archives" />
      <h1
        css={css`
          display: inline-block;
          font-family: Merriweather, Georgia, serif;
        `}
      >
        Archives
      </h1>
      <div>
        <p>Placeholder page</p>
      </div>
    </Layout>
  )
}
