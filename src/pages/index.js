import React from "react"
import Layout from "../components/layout"
import { Link, graphql } from "gatsby"
import { css } from "@emotion/core"
import SEO from "../components/seo"
import { rhythm } from "../utils/typography"

export default function Home({ data }) {
  return (
    <Layout>
      <SEO title="All posts" />
      <div
        css={css`
          font-family: Merriweather, Georgia, serif;
        `}
      >
        <h1
          css={css`
            display: inline-block;
            border-bottom: 1px solid;
            font-family: Merriweather, Georgia, serif;
          `}
        >
          Λm@zing Pandas Eating Things
        </h1>
        <h4>{data.allMarkdownRemark.totalCount} Posts</h4>
        {data.allMarkdownRemark.edges.map(({ node }) => (
          <div key={node.id}>
            <Link
              to={node.fields.slug}
              style={{ boxShadow: `` }}
              css={css`
                text-decoration: none;
                color: inherit;
              `}
            >
              <h3
                css={css`
                  margin-bottom: ${rhythm(1 / 4)};
                  margin-top: 2rem;
                  font-family: Merriweather, Georgia, serif;
                `}
              >
                {node.frontmatter.title}{" "}
              </h3>
              <small css={css``}>{node.frontmatter.date}</small>
              <p>{node.excerpt}</p>
            </Link>
          </div>
        ))}
      </div>
    </Layout>
  )
}

export const query = graphql`
  query {
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "MMMM DD, YYYY")
          }
          fields {
            slug
          }
          excerpt
        }
      }
    }
  }
`
