import React from "react"
import { Link, graphql } from "gatsby"
import { css } from "@emotion/core"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Bio from "../components/bio"
import { rhythm } from "../utils/typography"

// export default function BlogPost({ data }) {
class BlogPost extends React.Component {
  render() {
    const post = this.props.data.markdownRemark
    const { previous, next } = this.props.pageContext
    return (
      <Layout>
        <SEO title={post.frontmatter.title} description={post.excerpt} />
        <h1
          css={css`
            margin-top: 3rem;
            font-family: Merriweather, Georgia, serif;
          `}
        >
          {post.frontmatter.title}
        </h1>
        <span
          css={css`
            color: #bbb;
          `}
        >
          {" "}
          {post.frontmatter.date} · {post.timeToRead} min read
        </span>
        <div
          css={css`
            margin-top: 1rem;
            font-family: Merriweather, Georgia, serif;
          `}
          dangerouslySetInnerHTML={{ __html: post.html }}
        />
        <hr
          css={css`
            margin-top: ${rhythm(1)};
            margin-bottom: ${rhythm(1)};
          `}
        />
        <Bio />
        <ul
          style={{
            display: `flex`,
            flexWrap: `wrap`,
            justifyContent: `space-between`,
            listStyle: `none`,
            padding: 0,
          }}
        >
          <li>
            {previous && (
              <Link to={`${previous.fields.slug}`} rel="prev">
                ← {previous.frontmatter.title}
              </Link>
            )}
          </li>
          <li>
            {next && (
              <Link to={`${next.fields.slug}`} rel="next">
                {next.frontmatter.title} →
              </Link>
            )}
          </li>
        </ul>
      </Layout>
    )
  }
}

export default BlogPost

export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        date(formatString: "MMM DD, YYYY")
      }
      timeToRead
      excerpt
    }
  }
`
