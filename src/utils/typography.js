import Typography from "typography"
import kirkhamTheme from "typography-theme-kirkham"

// kirkhamTheme.overrideStyles = () => {
//   return {
//     h3: {
//       color: `#007acc`,
//       fontFamily: `font-family: Merriweather, Georgia, serif;`,
//     },
//   }
// }

const typography = new Typography(kirkhamTheme)

export default typography
export const rhythm = typography.rhythm
